# Proj0-Hello
-------------
- Author: Thomas Kismarton
- Function: Trivial project to exercise version control, turn-in, and other
mechanisms. Prints "Hello world".
- Contact email: tkismar2@cs.uoregon.edu

- Turn in with Canvas. The file you turn in is credentials.ini. We
   use the repository link in your credentials.ini to access the rest,
   just like the auto-checker.
